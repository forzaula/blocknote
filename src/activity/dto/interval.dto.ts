import {IsNotEmpty} from "class-validator";

export class DateDto{
    @IsNotEmpty()
    readonly start_date:string
    readonly end_date:string
}