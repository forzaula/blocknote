import {IsNotEmpty} from "class-validator";

export class CreateActivityDto{
  @IsNotEmpty()
  readonly name:string
  @IsNotEmpty()
  readonly description:string
  @IsNotEmpty()
  readonly location: number
  @IsNotEmpty()
  readonly day: string
  readonly account:number
}