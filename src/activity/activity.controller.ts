import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import { ActivityService } from './activity.service';
import { CreateActivityDto } from './dto/create-activity.dto';
import {DateDto} from "./dto/interval.dto";
import {JwtAuthGuard} from "../users/jwt-auth.guard";
import {Account} from "../users/users.entity";
import {User} from "../decorators/user.decorator";


@Controller('activity')
export class ActivityController {
  constructor(private readonly activityService:ActivityService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  @UseGuards(JwtAuthGuard)
  createActivity(@Body()dto:CreateActivityDto,@User()currentUser:Account){
    return this.activityService.createActivity(dto,currentUser)
  }
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  async deleteActivity(@Param('id') id:number,@User()currentUser:Account){
    return await this.activityService.deleteActivity(id,currentUser)
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  @UseGuards(JwtAuthGuard)
  async updateActivity(@Param('id') id:number,@Body() dto:CreateActivityDto,@User()currentUser:Account){
    return this.activityService.updateActivity(id,dto,currentUser)

  }
  @Get('activitydate')
  async availableActivity(@Body() dto:DateDto){
    return this.activityService.findActivityDto(dto)
  }

  @Get('findavailablelocation')
  async findAvailableLocation(@Body() dto:DateDto){
    return this.activityService.avalaibleLocationByDate(dto)
  }

}
