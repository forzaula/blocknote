import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import { LocationService } from './location.service';
import { CreateLocationDto } from './dto/create-location.dto';
import { Activity } from '../activity/activity.entity';
import {User} from "../decorators/user.decorator";
import {Account} from "../users/users.entity";
import {JwtAuthGuard} from "../users/jwt-auth.guard";


@Controller('location')
export class LocationController {
  constructor(private readonly locationService:LocationService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  createLocation(@Body() dto:CreateLocationDto,@User()currentUser:Account){
    return this.locationService.createLocation(dto,currentUser)

  }
  @Put(':id')
  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  async updateLocation(@Param('id') id:number,@Body() dto:CreateLocationDto,@User()currentUser:Account){
    return this.locationService.updateLocation(id,dto,currentUser)
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)

  async deleteLocation(@Param('id') id:number,@User()currentUser:Account){
    return await this.locationService.deleteLocation(id,currentUser)
  }
  @Get('availablelocation')
  async availableLocation(){
    return this.locationService.findAvailableLocation()
  }

  @Get(':id')
  async findByLocation(@Param('id') id:number){
    return await this.locationService.findByLocation(id)
  }

}
