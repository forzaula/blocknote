import {
  JoinColumn,
  Column,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
  ManyToOne
} from 'typeorm';
import { Activity } from '../activity/activity.entity';
import {Account} from "../users/users.entity";


@Entity()
export class Location{
  @PrimaryGeneratedColumn()
  id: number

  @Column({unique:true})
  address:string


  @OneToMany(type=>Activity,activity=>activity.location)
  @JoinColumn()
  activities:Activity[]

  @ManyToOne(type => Account, account => account.locations,{eager:true})
  @JoinTable()
  account: Account;

}