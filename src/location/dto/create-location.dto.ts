import { Activity } from '../../activity/activity.entity';
import {IsNotEmpty} from "class-validator";

export class CreateLocationDto{
  @IsNotEmpty()
  readonly address:string
  readonly activities: number
  readonly account: number
}