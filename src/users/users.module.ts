import {forwardRef, Module} from '@nestjs/common';
import {UsersController} from "./users.controller";
import {UsersService} from "./users.service";
import {JwtModule} from "@nestjs/jwt";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Account} from "./users.entity";
import {JwtAuthGuard} from "./jwt-auth.guard";

@Module({
    controllers: [UsersController],
    providers: [UsersService,JwtModule,JwtAuthGuard],
    imports: [
        forwardRef(()=> UsersModule),
        TypeOrmModule.forFeature([Account]),
        JwtModule.register({
            secret: process.env.PRIVATE_KEY || 'SECRET',
            signOptions: {
                expiresIn: '24h'
            }
        }),
    ],
    exports: [
        JwtModule,JwtAuthGuard,UsersService
    ]
})
export class UsersModule {}
