import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    OneToMany,
    JoinColumn,
    ManyToMany,
    JoinTable
} from "typeorm";
import {Activity} from "../activity/activity.entity";
import {Location} from "../location/location.entity";

@Entity()
export class Account extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number

    @Column({unique: true})
    username: string

    @Column({unique: true})
    email: string

    @Column()
    password: string

    @OneToMany(()=>Activity,(activity)=>activity.account)
    @JoinColumn()
    activities:Activity[]

    @OneToMany(type => Location, location => location.account)
    @JoinTable()
    locations: Location[];
}