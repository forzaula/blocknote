import {Body, Controller, Post, UsePipes, ValidationPipe} from '@nestjs/common';
import {CreateUserDto} from "./dto/create-user.dto";
import {UsersService} from "./users.service";

import {JwtAuthGuard} from "./jwt-auth.guard";
import {LoginUserDto} from "./dto/login-user.dto";

@Controller('auth')
export class UsersController {

    constructor(private usersService: UsersService) {
    }

    @UsePipes(new ValidationPipe())
    @Post('/login')
    login(@Body() userDto: LoginUserDto){
        return this.usersService.login(userDto)
    }
    @UsePipes(new ValidationPipe())
    @Post('/registration')
    registration(@Body() userDto: CreateUserDto){
        return this.usersService.registration(userDto)
    }
}
