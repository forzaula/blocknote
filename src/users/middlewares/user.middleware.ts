// import {HttpException, HttpStatus, Injectable, NestMiddleware, UnauthorizedException} from "@nestjs/common";
// import {NextFunction,Request,Response} from "express";
// import {UsersService} from '../users.service'
//
// @Injectable()
// export class userMiddleware implements NestMiddleware{
//     constructor(private userService: UsersService) {
//     }
//     async use(req:Request,res:Response,next:NextFunction){
//         const {authorization} = req.headers;
//         if(!authorization) {
//             throw new UnauthorizedException('Invalid Token')
//         }
//         const token = authorization.split(' ')[1]
//         if(!token) {
//             throw new HttpException('Token jok', HttpStatus.NOT_FOUND);
//         }
//         const user = await this.userService.verifyUser(token);
//         if(!user) {
//             throw new HttpException('not_found', HttpStatus.NOT_FOUND);
//         }
//         next()
//     }
//
// }